---
title: "Rater Analysis Report"
author: "Huye Zhou"
output:
  pdf_document:
    toc: yes
    toc_depth: '3'
  html_document:
    toc: yes
    toc_depth: '3'
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo=FALSE, message=FALSE, warning=FALSE)
```

```{r package}
library(plyr)
library(tidyverse)
library(boot)
library(knitr)
set.seed(123)
df <- read_csv("data/case_by_case_summary.csv")
n=1000
```

```{r}
# df <- df[,1:5]
df <- drop_na(df)
df <- rename(df, reader_id=`Reader`,
             image_type=`UNAIDED/AIDED`,
             CAD_Markings=`CAD Markings`,
             Reader_Markings=`Reader Markings`,
             is_cancer=`Cancer/Normal`,
             rad_score=`Initial BIRADS`
             )
df$is_cancer <- 2-as.numeric(as.factor(df$is_cancer))
df$rad_score <- 1*(df$rad_score==0)
# kable(group_by(df, image_type) %>% summarise(count = n(), .groups = 'drop'))
```


```{r}
df_reader_score <- spread(df[,c(1:5,8)], image_type, rad_score)
# df_reader_score[as.logical(rowSums(is.na(df_reader_score))),]
df_reader_score <- drop_na(df_reader_score)
df_reader_marking <- spread(df[,c(1:5,7)], image_type, Reader_Markings)
df_reader_marking <- drop_na(df_reader_marking)
df_processed <- merge(df_reader_score,df_reader_marking,by=names(df_reader_score)[1:4])
```

```{r}
p_diff_1 <- function(results_unaided, results_aided) {
  p <-
    pnorm(results_aided[["t0"]],
          mean = results_unaided[["t0"]],
          sd = sd(results_unaided[["t"]]))
  if (p > 0.5) {
    p <- 1 - p
  }
  return(2*p)
}
p_diff_2 <- function(results_unaided, results_aided){
  res <- t.test(results_unaided[["t"]],
                results_aided[["t"]],
                paired = TRUE,
                alternative = "two.sided")
  return(res$p.value)
}
```


# AUC
## Bootstrapping
```{r}
df_AUC <- df_processed %>%
  group_by(reader_id) %>%
  summarise(unaided_mean=0,
            unaided_low=0,
            unaided_high=0,
            aided_mean=0,
            aided_low=0,
            aided_high=0,
            p_value_1=0,
            p_value_2=0,
            .groups = 'drop'
            )
AUC <- function(data, indices) {
  data <- data[indices,]
  TP <- nrow(data %>% filter(detection=="True-Positive"&score==1))
  total <- nrow(data %>% filter(is_cancer==1))
  TPR <- TP/total
  TN <- nrow(data %>% filter(is_cancer==0&score==0))
  total <- nrow(data %>% filter(is_cancer==0))
  specificity <-  TN/total
  FPR <- 1 - specificity
  auc <- 0.5*FPR*TPR + specificity*TPR
  return(auc)
}

for (i in 1:16){
  reader_aided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, AIDED.x, AIDED.y) %>% 
    rename(score=AIDED.x, detection=AIDED.y)
  reader_unaided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, UNAIDED.x, UNAIDED.y) %>% 
    rename(score=UNAIDED.x, detection=UNAIDED.y)
  results_unaided <- boot(data=reader_unaided, statistic=AUC, R=n)
  results_aided <- boot(data=reader_aided, statistic=AUC, R=n)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_AUC[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_AUC[i,"aided_mean"] <- ci_aided[["t0"]]
  df_AUC[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_AUC[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_AUC[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_AUC[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_AUC[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
  df_AUC[i,"p_value_2"] <- p_diff_2(results_unaided, results_aided)
}
df_AUC_group <- df_AUC
df_AUC_group[c(17,18),] <- NA
df_AUC_group$reader_id <- as.character(df_AUC_group$reader_id)
for (j in c("A","B")){
  i <- i+1
  reader_aided <- 
    df_processed %>% 
    filter(Group==j) %>% 
    select(is_cancer, AIDED.x, AIDED.y) %>% 
    rename(score=AIDED.x, detection=AIDED.y)
  reader_unaided <- 
    df_processed %>% 
    filter(Group==j) %>% 
    select(is_cancer, UNAIDED.x, UNAIDED.y) %>% 
    rename(score=UNAIDED.x, detection=UNAIDED.y)
  results_unaided <- boot(data=reader_unaided, statistic=AUC, R=2000)
  results_aided <- boot(data=reader_aided, statistic=AUC, R=2000)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_AUC_group[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_AUC_group[i,"aided_mean"] <- ci_aided[["t0"]]
  df_AUC_group[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_AUC_group[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_AUC_group[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_AUC_group[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_AUC_group[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
  df_AUC_group[i,"p_value_2"] <- p_diff_2(results_unaided, results_aided)
  df_AUC_group[i,1] <- j
}

```

```{r,results='asis'}
kable(df_AUC_group, caption = "bootstrapping results for AUC")
```


## Performing statistical tests
### Paired t test (one tailed)

```{r}
res <- t.test(df_AUC$unaided_mean, 
              df_AUC$aided_mean, 
              paired = TRUE, 
              alternative = "less")
```

The p value is `r res$p.value`, which is smaller than the significance level $\alpha=0.05$. We can conclude that the marking have significant positive effect on reader's AUC from paired t test. 

### McNemar's test for each reader

```{r}
df_processed <- df_processed %>% 
  mutate(AIDED=1*(AIDED.y=="True-Positive" & AIDED.x ==1 & is_cancer==1)+ 1*(AIDED.y=="True-Negative" & is_cancer==0),
         UNAIDED=1*(UNAIDED.y=="True-Positive" & UNAIDED.x ==1 & is_cancer==1)+1*(UNAIDED.y=="True-Negative" & is_cancer==0))
mcnemar_results <- 
  df_AUC_group %>% 
  select(reader_id,p_value_1,p_value_2) %>% 
  mutate(McNemar_p_value=0)
for (i in 1:16){
  tb <- with(df_processed %>% filter(reader_id==i), table(AIDED, UNAIDED))
  mcnemar_results$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
for (j in c("A","B")){
  i <- i+1
  tb <- with(df_processed %>% filter(Group==j), table(AIDED, UNAIDED))
  mcnemar_results$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
```

```{r,results='asis'}
kable(mcnemar_results, caption = "p value of all tests for each reader's AUC")
```
We can see that **reader 7**, **reader 8**, **Group A** have significant different results after adding markings on images. Let's see the two-way table for these two readers for unaided and aided.

#### Reader 7

```{r}
with(df_processed %>% filter(reader_id==7), table(AIDED, UNAIDED))
```

We can see that the marking have a negative effect on **reader 7**'s AUC.

#### Reader 8

```{r}
with(df_processed %>% filter(reader_id==8), table(AIDED, UNAIDED))
```

We can see that the marking have a positive effect on **reader 8**'s AUC.

#### Group A

```{r}
with(df_processed %>% filter(Group=="A"), table(AIDED, UNAIDED))
```

We can see that the marking have a positive effect on **Group A**'s AUC.

# CDR
## Bootstrapping
```{r}
df_CDR <- df_processed %>%
  group_by(reader_id) %>%
  summarise(unaided_mean=0,
            unaided_low=0,
            unaided_high=0,
            aided_mean=0,
            aided_low=0,
            aided_high=0,
            p_value_1=0,
            p_value_2=0,
            .groups = 'drop'
            )
CDR <- function(data, indices) {
  data <- data[indices,]
  TP=nrow(data %>% filter(detection=="True-Positive"&score==1))
  total=nrow(data %>% filter(is_cancer==1))
  return(TP/total)
}

for (i in 1:16){
  reader_aided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, AIDED.x, AIDED.y) %>% 
    rename(score=AIDED.x, detection=AIDED.y)
  reader_unaided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, UNAIDED.x, UNAIDED.y) %>% 
    rename(score=UNAIDED.x, detection=UNAIDED.y)
  results_unaided <- boot(data=reader_unaided, statistic=CDR, R=n)
  results_aided <- boot(data=reader_aided, statistic=CDR, R=n)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_CDR[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_CDR[i,"aided_mean"] <- ci_aided[["t0"]]
  df_CDR[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_CDR[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_CDR[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_CDR[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_CDR[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
  df_CDR[i,"p_value_2"] <- p_diff_2(results_unaided, results_aided)
}
df_CDR_group <- df_CDR
df_CDR_group[c(17,18),] <- NA
df_CDR_group$reader_id <- as.character(df_CDR_group$reader_id)
for (j in c("A","B")){
  i <- i+1
  reader_aided <- 
    df_processed %>% 
    filter(Group==j) %>% 
    select(is_cancer, AIDED.x, AIDED.y) %>% 
    rename(score=AIDED.x, detection=AIDED.y)
  reader_unaided <- 
    df_processed %>% 
    filter(Group==j) %>% 
    select(is_cancer, UNAIDED.x, UNAIDED.y) %>% 
    rename(score=UNAIDED.x, detection=UNAIDED.y)
  results_unaided <- boot(data=reader_unaided, statistic=CDR, R=2000)
  results_aided <- boot(data=reader_aided, statistic=CDR, R=2000)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_CDR_group[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_CDR_group[i,"aided_mean"] <- ci_aided[["t0"]]
  df_CDR_group[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_CDR_group[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_CDR_group[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_CDR_group[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_CDR_group[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
  df_CDR_group[i,"p_value_2"] <- p_diff_2(results_unaided, results_aided)
  df_CDR_group[i,1] <- j
}
```

```{r,results='asis'}
kable(df_CDR_group, caption = "bootstrapping results for CDR")
```


## Performing statistical tests
### Paired t test (one tailed)

```{r}
res <- t.test(df_CDR$unaided_mean, 
              df_CDR$aided_mean, 
              paired = TRUE, 
              alternative = "less")
```

The p value is `r res$p.value`, which is larger than the significance level $\alpha=0.05$. We can conclude that the marking doesn't have significant positive effect on reader's results from paired t test. 

### McNemar's test for each reader

```{r}
df_processed <- df_processed %>% 
  mutate(AIDED=1*(AIDED.y=="True-Positive" & AIDED.x ==1),
         UNAIDED=1*(UNAIDED.y=="True-Positive" & UNAIDED.x ==1))
mcnemar_results <- 
  df_CDR_group %>% 
  select(reader_id,p_value_1,p_value_2) %>% 
  mutate(McNemar_p_value=0)
for (i in 1:16){
  tb <- with(df_processed %>% filter(reader_id==i,is_cancer==1), table(AIDED, UNAIDED))
  mcnemar_results$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
for (j in c("A","B")){
  i <- i+1
  tb <- with(df_processed %>% filter(Group==j,is_cancer==1), table(AIDED, UNAIDED))
  mcnemar_results$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
```

```{r,results='asis'}
kable(mcnemar_results, caption = "p value of all tests for each reader's CDR")
```

We can see that **reader 9**, **reader 11**, **Group B** have significant different results after adding markings on images. Let's see the two-way table for these two readers for unaided and aided.

#### Reader 9

```{r}
with(df_processed %>% filter(reader_id==9,is_cancer==1), table(AIDED, UNAIDED))
```

We can see that the marking can improve **reader 9**'s detection of cancer.

#### Reader 11

```{r}
with(df_processed %>% filter(reader_id==11,is_cancer==1), table(AIDED, UNAIDED))
```

We can see that the marking can improve **reader 11**'s detection of cancer.

#### Group B

```{r}
with(df_processed %>% filter(Group=="B",is_cancer==1), table(AIDED, UNAIDED))
```

We can see that the marking can improve **Group B**'s detection of cancer.


# Specificity
## Bootstrapping
```{r}
df_Specificity <- df_processed %>%
  group_by(reader_id) %>%
  summarise(unaided_mean=0,
            unaided_low=0,
            unaided_high=0,
            aided_mean=0,
            aided_low=0,
            aided_high=0,
            p_value_1=0,
            p_value_2=0,
            .groups = 'drop'
            )
Specificity <- function(data, indices) {
  data <- data[indices,]
  TN=nrow(data %>% filter(is_cancer==0&score==0))
  total=nrow(data %>% filter(is_cancer==0))
  return(TN/total)
}

for (i in 1:16){
  reader_aided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, AIDED.x) %>% 
    rename(score=AIDED.x)
  reader_unaided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, UNAIDED.x) %>% 
    rename(score=UNAIDED.x)
  results_unaided <- boot(data=reader_unaided, statistic=Specificity, R=n)
  results_aided <- boot(data=reader_aided, statistic=Specificity, R=n)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_Specificity[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_Specificity[i,"aided_mean"] <- ci_aided[["t0"]]
  df_Specificity[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_Specificity[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_Specificity[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_Specificity[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_Specificity[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
  df_Specificity[i,"p_value_2"] <- p_diff_2(results_unaided, results_aided)
}

df_Specificity_group <- df_Specificity
df_Specificity_group[c(17,18),] <- NA
df_Specificity_group$reader_id <- as.character(df_Specificity_group$reader_id)
for (j in c("A","B")){
  i <- i+1
  reader_aided <- 
    df_processed %>% 
    filter(Group==j) %>% 
    select(is_cancer, AIDED.x) %>% 
    rename(score=AIDED.x)
  reader_unaided <- 
    df_processed %>% 
    filter(Group==j) %>% 
    select(is_cancer, UNAIDED.x) %>% 
    rename(score=UNAIDED.x)
  results_unaided <- boot(data=reader_unaided, statistic=Specificity, R=2000)
  results_aided <- boot(data=reader_aided, statistic=Specificity, R=2000)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_Specificity_group[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_Specificity_group[i,"aided_mean"] <- ci_aided[["t0"]]
  df_Specificity_group[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_Specificity_group[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_Specificity_group[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_Specificity_group[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_Specificity_group[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
  df_Specificity_group[i,"p_value_2"] <- p_diff_2(results_unaided, results_aided)
  df_Specificity_group[i,1] <- j
}
```

```{r,results='asis'}
kable(df_Specificity_group, caption = "bootstrapping results for Specificity")
```


## Performing statistical tests
### Paired t test (one tailed)

```{r}
res <- t.test(df_Specificity$unaided_mean, 
              df_Specificity$aided_mean, 
              paired = TRUE, 
              alternative = "less")
```

The p value is `r res$p.value`, which is larger than the significance level $\alpha=0.05$. We can conclude that the marking doesn't have significant positive effect on reader's Specificity from paired t test. 

### McNemar's test for each reader

```{r}
df_processed <- df_processed %>% 
  mutate(AIDED=AIDED.x,
         UNAIDED=UNAIDED.x)
df_processed$AIDED <- as.factor(df_processed$AIDED)
df_processed$UNAIDED <- as.factor(df_processed$UNAIDED)
mcnemar_results <- 
  df_Specificity_group %>% 
  select(reader_id,p_value_1,p_value_2) %>% 
  mutate(McNemar_p_value=0)
for (i in 1:16){
  tb <- with(df_processed %>% filter(reader_id==i,is_cancer==0), table(AIDED, UNAIDED))
  mcnemar_results$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
for (j in c("A","B")){
  i <- i+1
  tb <- with(df_processed %>% filter(Group==j,is_cancer==0), table(AIDED, UNAIDED))
  mcnemar_results$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
```

```{r,results='asis'}
kable(mcnemar_results, caption = "p value of all tests for each reader's Specificity")
```

We can see that **reader 4**, **reader 8**, **reader 8**, **Group A**, and **Group B** have significant different results after adding markings on images. Let's see the two-way table for these 3 patients for unaided and aided.

#### Reader 4

```{r}
with(df_processed %>% filter(reader_id==4,is_cancer==0), table(AIDED, UNAIDED))
```

We can see that the marking will make **reader 4**'s detection of normal case better.

#### Reader 8

```{r}
with(df_processed %>% filter(reader_id==8,is_cancer==0), table(AIDED, UNAIDED))
```

We can see that the marking will make **reader 8**'s detection of normal case better.

#### Reader 9

```{r}
with(df_processed %>% filter(reader_id==9,is_cancer==0), table(AIDED, UNAIDED))
```

We can see that the marking will make **reader 9**'s detection of normal case worse.

#### Group A

```{r}
with(df_processed %>% filter(Group=="A",is_cancer==0), table(AIDED, UNAIDED))
```

We can see that the marking will make **Group A**'s detection of normal case better.

#### Group B 

```{r}
with(df_processed %>% filter(Group=="B",is_cancer==0), table(AIDED, UNAIDED))
```

We can see that the marking will make **Group B **'s detection of normal case worse.

# Recall Rate
## Bootstrapping
```{r}
df_RecallRate <- df_processed %>%
  group_by(reader_id) %>%
  summarise(unaided_mean=0,
            unaided_low=0,
            unaided_high=0,
            aided_mean=0,
            aided_low=0,
            aided_high=0,
            p_value_1=0,
            p_value_2=0,
            .groups = 'drop'
            )
RecallRate <- function(data, indices) {
  data <- data[indices,]
  RC=nrow(data %>% filter(score==1))
  total=nrow(data)
  return(RC/total)
}

for (i in 1:16){
  reader_aided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, AIDED.x) %>% 
    rename(score=AIDED.x)
  reader_unaided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, UNAIDED.x) %>% 
    rename(score=UNAIDED.x)
  results_unaided <- boot(data=reader_unaided, statistic=RecallRate, R=n)
  results_aided <- boot(data=reader_aided, statistic=RecallRate, R=n)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_RecallRate[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_RecallRate[i,"aided_mean"] <- ci_aided[["t0"]]
  df_RecallRate[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_RecallRate[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_RecallRate[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_RecallRate[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_RecallRate[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
  df_RecallRate[i,"p_value_2"] <- p_diff_2(results_unaided, results_aided)
}
df_RecallRate_group <- df_RecallRate
df_RecallRate_group[c(17,18),] <- NA
df_RecallRate_group$reader_id <- as.character(df_RecallRate_group$reader_id)
for (j in c("A","B")){
  i <- i+1
  reader_aided <- 
    df_processed %>% 
    filter(Group==j) %>% 
    select(is_cancer, AIDED.x) %>% 
    rename(score=AIDED.x)
  reader_unaided <- 
    df_processed %>% 
    filter(Group==j) %>% 
    select(is_cancer, UNAIDED.x) %>% 
    rename(score=UNAIDED.x)
  results_unaided <- boot(data=reader_unaided, statistic=RecallRate, R=2000)
  results_aided <- boot(data=reader_aided, statistic=RecallRate, R=2000)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_RecallRate_group[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_RecallRate_group[i,"aided_mean"] <- ci_aided[["t0"]]
  df_RecallRate_group[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_RecallRate_group[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_RecallRate_group[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_RecallRate_group[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_RecallRate_group[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
  df_RecallRate_group[i,"p_value_2"] <- p_diff_2(results_unaided, results_aided)
  df_RecallRate_group[i,1] <- j
}
```

```{r,results='asis'}
kable(df_RecallRate_group, caption = "bootstrapping results for Recall Rate")
```


## Performing statistical tests
### Paired t test (one tailed)

```{r}
res <- t.test(df_RecallRate$unaided_mean, 
              df_RecallRate$aided_mean, 
              paired = TRUE, 
              alternative = "less")
```

The p value is `r res$p.value`, which is larger than the significance level $\alpha=0.05$. We can conclude that the marking doesn't have significant positive effect on reader's results from paired t test. 

### McNemar's test for each reader

```{r}
df_processed <- df_processed %>% 
  mutate(AIDED=AIDED.x,
         UNAIDED=UNAIDED.x)
df_processed$AIDED <- as.factor(df_processed$AIDED)
df_processed$UNAIDED <- as.factor(df_processed$UNAIDED)
mcnemar_results <- 
  df_RecallRate_group %>% 
  select(reader_id,p_value_1,p_value_2) %>% 
  mutate(McNemar_p_value=0)
for (i in 1:16){
  tb <- with(df_processed %>% filter(reader_id==i), table(AIDED, UNAIDED))
  mcnemar_results$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
for (j in c("A","B")){
  i <- i+1
  tb <- with(df_processed %>% filter(Group==j), table(AIDED, UNAIDED))
  mcnemar_results$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
```

```{r,results='asis'}
kable(mcnemar_results, caption = "p value of all tests for each reader's Recall Rate")
```

We can see that **reader 1**, **reader 7**, **reader 8**, **reader 9**, **reader 11**, **reader 12**, **Group A**, and **Group B** have significant different results after adding markings on images.

#### Reader 1

```{r}
with(df_processed %>% filter(reader_id==1), table(AIDED, UNAIDED))
```

We can see that the marking will make **reader 1** recall more.

#### Reader 7

```{r}
with(df_processed %>% filter(reader_id==7), table(AIDED, UNAIDED))
```

We can see that the marking will make **reader 7** recall more.

#### Reader 8

```{r}
with(df_processed %>% filter(reader_id==8), table(AIDED, UNAIDED))
```

We can see that the marking will make **reader 8** recall less.

#### Reader 9

```{r}
with(df_processed %>% filter(reader_id==9), table(AIDED, UNAIDED))
```

We can see that the marking will make **reader 9** recall more.

#### Reader 11

```{r}
with(df_processed %>% filter(reader_id==11), table(AIDED, UNAIDED))
```

We can see that the marking will make **reader 11** recall more.

#### Reader 12

```{r}
with(df_processed %>% filter(reader_id==12), table(AIDED, UNAIDED))
```

We can see that the marking will make **reader 12** recall more.

#### Group A

```{r}
with(df_processed %>% filter(Group=="A"), table(AIDED, UNAIDED))
```

We can see that the marking will make **Group A** recall less.

#### Group B

```{r}
with(df_processed %>% filter(Group=="B"), table(AIDED, UNAIDED))
```

We can see that the marking will make **Group B** recall more.

# Test with difficulty and density groups

```{r}
set.seed(123)
df <- read_csv("data/case_by_case_summary_density_ALL_complexity_ALL.csv")
n=1000
```

```{r}
df <- drop_na(df)
df <- rename(df, 
             reader_id = `Reader`,
             image_type = `UNAIDED/AIDED`,
             CAD_Markings = `CAD Markings`,
             Reader_Markings = `Reader Markings`,
             is_cancer = `Cancer/Normal`,
             rad_score = `Initial BIRADS`)
df$is_cancer <- 2-as.numeric(as.factor(df$is_cancer))
df$rad_score <- 1*(df$rad_score==0)

df_difficulty <- df %>% 
  group_by(MRN, Complexity) %>% 
  summarise(count = n(), .groups = 'drop') %>% 
  spread(Complexity, count) %>% 
  rename(High=`High or Busy`)
df_difficulty[is.na(df_difficulty)] <- 0
df_difficulty$difficulty <-
  colnames(df_difficulty[,2:4])[apply(df_difficulty[,2:4],1,which.max)]

df_Density <- df %>% 
  group_by(MRN, Density) %>% 
  summarise(count = n(), .groups = 'drop') %>% 
  spread(Density, count)
df_Density[is.na(df_Density)] <- 0
df_Density$Density <-
  colnames(df_Density[,2:5])[apply(df_Density[,2:5],1,which.max)]

df_Density <- df_Density %>% mutate(nondense=A+B, dense=C+D)
df_Density$Density_group <-
  colnames(df_Density[,7:8])[apply(df_Density[,7:8],1,which.max)]

df_reader_score <- spread(df[,c(1:5,8)], image_type, rad_score)
df_reader_score <- drop_na(df_reader_score)
df_reader_marking <- spread(df[,c(1:5,7)], image_type, Reader_Markings)
df_reader_marking <- drop_na(df_reader_marking)
df_processed <- merge(df_reader_score,df_reader_marking,by=names(df_reader_score)[1:4])
df_processed$difficulty <- mapvalues(df_processed$MRN, 
                           from = df_difficulty$MRN, 
                           to = df_difficulty$difficulty)
df_processed$Density <- mapvalues(df_processed$MRN, 
                           from = df_Density$MRN, 
                           to = df_Density$Density)
df_processed$Density_group <- mapvalues(df_processed$MRN, 
                           from = df_Density$MRN, 
                           to = df_Density$Density_group)
```

## McNemar’s test for each reader
```{r}
mcnemar.CDR.readers <- function(df_processed) {
  df_processed <- df_processed %>%
    mutate(AIDED = 1 * (AIDED.y == "True-Positive" & AIDED.x == 1),
           UNAIDED = 1 * (UNAIDED.y == "True-Positive" & UNAIDED.x == 1))
  mcnemar_results <- tibble(reader_id = c(1:16), p_value = 0)
  df_processed$AIDED <- as.factor(df_processed$AIDED)
  df_processed$UNAIDED <- as.factor(df_processed$UNAIDED)
  for (i in 1:16) {
    tb <- with(df_processed %>% filter(reader_id == i, is_cancer == 1),
               table(AIDED, UNAIDED))
    mcnemar_results$p_value[i] <- mcnemar.test(tb)[["p.value"]]
  }
  return(mcnemar_results)
}

p_readers <- tibble(reader_id = c(1:16), 
                    Low = 0, Medium=0, High=0,
                    A = 0, B=0, C=0, D=0)
for (i in c("Low", "Medium", "High")){
  p_readers[i] <- 
    mcnemar.CDR.readers(df_processed %>% filter(difficulty==i))$p_value
}
for (i in c("A", "B", "C", "D")){
  p_readers[i] <- 
    mcnemar.CDR.readers(df_processed %>% filter(Density==i))$p_value
}
p_readers[is.na(p_readers)] <- 1
```


```{r,results='asis'}
kable(p_readers, caption = "p values of McNemar's test for each reader's CDR grouped by difficulty and density", digits=3)
```

## McNemar’s test for difficulty and density groups

```{r}
mcnemar.CDR <- function(df_processed) {
  df_processed <- df_processed %>%
    mutate(AIDED = 1 * (AIDED.y == "True-Positive" & AIDED.x == 1),
           UNAIDED = 1 * (UNAIDED.y == "True-Positive" & UNAIDED.x == 1))
  df_processed$AIDED <- as.factor(df_processed$AIDED)
  df_processed$UNAIDED <- as.factor(df_processed$UNAIDED)
  tb <- with(df_processed %>% filter(is_cancer == 1),
             table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}

mcnemar.Specificity <- function(df_processed) {
  df_processed <- df_processed %>%
    mutate(AIDED = AIDED.x,
           UNAIDED = UNAIDED.x)
  df_processed$AIDED <- as.factor(df_processed$AIDED)
  df_processed$UNAIDED <- as.factor(df_processed$UNAIDED)
  tb <- with(df_processed %>% filter(is_cancer == 0),
             table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}

mcnemar.RecallRate <- function(df_processed) {
  df_processed <- df_processed %>%
    mutate(AIDED = AIDED.x,
           UNAIDED = UNAIDED.x)
  df_processed$AIDED <- as.factor(df_processed$AIDED)
  df_processed$UNAIDED <- as.factor(df_processed$UNAIDED)
  tb <- with(df_processed, table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}

dense_groups <- c("nondense", "dense")
p_dense_group <- tibble(group = dense_groups, 
                        CDR=0, Specificity=0, RecallRate=0)
for (i in 1:2){
  p_dense_group[i, "CDR"] <- 
    mcnemar.CDR(df_processed %>% filter(Density_group==dense_groups[i]))
  p_dense_group[i, "Specificity"] <- 
    mcnemar.Specificity(df_processed %>% filter(Density_group==dense_groups[i]))
  p_dense_group[i, "RecallRate"] <- 
    mcnemar.RecallRate(df_processed %>% filter(Density_group==dense_groups[i]))
}
p_dense_group[is.na(p_dense_group)] <- 1

difficulty_group <- c("Low", "Medium", "High")
p_difficulty_group <- tibble(group = difficulty_group, 
                        CDR=0, Specificity=0, RecallRate=0)
for (i in 1:3){
  p_difficulty_group[i, "CDR"] <- 
    mcnemar.CDR(df_processed %>% filter(difficulty==difficulty_group[i]))
  p_difficulty_group[i, "Specificity"] <- 
    mcnemar.Specificity(df_processed %>% filter(difficulty==difficulty_group[i]))
  p_difficulty_group[i, "RecallRate"] <- 
    mcnemar.RecallRate(df_processed %>% filter(difficulty==difficulty_group[i]))
}
p_difficulty_group[is.na(p_difficulty_group)] <- 1
```

```{r}
p_low_group_dense <- tibble(group = dense_groups, 
                            CDR=0, Specificity=0, RecallRate=0)
for (i in 1:2){
  p_low_group_dense[i, "CDR"] <- 
    mcnemar.CDR(df_processed %>%
                  filter(difficulty=="Low" & 
                           Density_group==dense_groups[i]))
  p_low_group_dense[i, "Specificity"] <- 
    mcnemar.Specificity(df_processed %>% 
                          filter(difficulty=="Low" &
                                   Density_group==dense_groups[i]))
  p_low_group_dense[i, "RecallRate"] <- 
    mcnemar.RecallRate(df_processed %>% 
                         filter(difficulty=="Low" & 
                                         Density_group==dense_groups[i]))
}
p_low_group_dense[is.na(p_low_group_dense)] <- 1


p_medium_group_dense <- tibble(group = dense_groups, 
                               CDR=0, Specificity=0, RecallRate=0)
for (i in 1:2){
  p_medium_group_dense[i, "CDR"] <- 
    mcnemar.CDR(df_processed %>%
                  filter(difficulty=="Medium" & 
                           Density_group==dense_groups[i]))
  p_medium_group_dense[i, "Specificity"] <- 
    mcnemar.Specificity(df_processed %>% 
                          filter(difficulty=="Medium" &
                                   Density_group==dense_groups[i]))
  p_medium_group_dense[i, "RecallRate"] <- 
    mcnemar.RecallRate(df_processed %>% 
                         filter(difficulty=="Medium" & 
                                         Density_group==dense_groups[i]))
}
p_medium_group_dense[is.na(p_medium_group_dense)] <- 1


p_high_group_dense <- tibble(group = dense_groups, 
                               CDR=0, Specificity=0, RecallRate=0)

for (i in 2){
  p_high_group_dense[i, "CDR"] <- 
    mcnemar.CDR(df_processed %>%
                  filter(difficulty=="High" & 
                           Density_group==dense_groups[i]))
  p_high_group_dense[i, "Specificity"] <- 
    mcnemar.Specificity(df_processed %>% 
                          filter(difficulty=="High" &
                                   Density_group==dense_groups[i]))
  p_high_group_dense[i, "RecallRate"] <- 
    mcnemar.RecallRate(df_processed %>% 
                         filter(difficulty=="High" & 
                                         Density_group==dense_groups[i]))
}
for (i in 1){
  p_high_group_dense[i, "Specificity"] <- 
    mcnemar.Specificity(df_processed %>% 
                          filter(difficulty=="High" &
                                   Density_group==dense_groups[i]))
  p_high_group_dense[i, "RecallRate"] <- 
    mcnemar.RecallRate(df_processed %>% 
                         filter(difficulty=="High" & 
                                         Density_group==dense_groups[i]))
}
p_high_group_dense[1,2] <- NA
```


```{r,results='asis'}
kable(p_dense_group, caption = "p values of McNemar's test for CDR, Specificity, and RecallRate grouped by density", digits=3)
```

```{r,results='asis'}
kable(p_difficulty_group, caption = "p values of McNemar's test for CDR, Specificity, and RecallRate grouped by difficulty", digits=3)
```

```{r,results='asis'}
kable(p_low_group_dense, caption = "Low difficulty group broken down by density", digits=3)
```

```{r,results='asis'}
kable(p_medium_group_dense, caption = "Medium difficulty group broken down by density", digits=3)
```

```{r,results='asis'}
kable(p_high_group_dense, caption = "High difficulty group broken down by density", digits=3)
```



