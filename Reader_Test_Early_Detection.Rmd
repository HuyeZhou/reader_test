---
title: "Reader Test for Early Detection"
author: "Huye Zhou"
output:
  pdf_document:
    toc: yes
    toc_depth: '3'
  html_document:
    toc: yes
    toc_depth: '3'
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo=FALSE, message=FALSE, warning=FALSE)
```
\newpage
```{r package}
library(plyr)
library(tidyverse)
library(boot)
library(knitr)
library(ggplot2)
library(gridExtra)

set.seed(123)
df <- read_csv("data/early_detection_all_cases.csv")
n=500
```

```{r}
df <- df %>% mutate(is_cancer = 1*(! lesion_type == "normal"))
df_processed = as_tibble(matrix(numeric(),nrow = 0, ncol = 7, dimnames=list(NULL, c("patient_name","accession_number", "lesion_type","is_cancer","UNAIDED","AIDED","reader_id"))))
for (i in 1:7){
  temp <- df[,c(1:3,32,(i*4):(i*4+3))]
  names(temp)[5:8] <- c("UNAIDED.TP","UNAIDED.TN","AIDED.TP","AIDED.TN")
  temp[is.na(temp)]=FALSE
  temp <- temp %>% mutate(UNAIDED = 1*is_cancer*UNAIDED.TP + 2*is_cancer*!UNAIDED.TP)
  temp <- temp %>% mutate(UNAIDED = UNAIDED + 3*(1-is_cancer)*UNAIDED.TN + 4*(1-is_cancer)*!UNAIDED.TN)
  temp$UNAIDED <- recode(temp$UNAIDED, `1`="TP", `2`="FN", `3`="TN", `4`="FP")
  temp <- temp %>% mutate(AIDED = 1*is_cancer*AIDED.TP + 2*is_cancer*!AIDED.TP)
  temp <- temp %>% mutate(AIDED = AIDED + 3*(1-is_cancer)*AIDED.TN + 4*(1-is_cancer)*!AIDED.TN)
  temp$AIDED <- recode(temp$AIDED, `1`="TP", `2`="FN", `3`="TN", `4`="FP")
  temp <- temp %>% select(patient_name,accession_number,lesion_type,is_cancer,UNAIDED,AIDED)
  temp <- temp %>% mutate(reader_id=i)
  df_processed <- rbind(df_processed, temp)
}
```

```{r}
p_diff_1 <- function(results_unaided, results_aided) {
  p <-
    pnorm(results_aided[["t0"]],
          mean = results_unaided[["t0"]],
          sd = sd(results_unaided[["t"]]))
  if (p > 0.5) {
    p <- 1 - p
  }
  return(2*p)
}
g_legend<-function(a.gplot) {
  tmp <- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x)
    x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}

mcnemar.readers.p <- function(df_processed, test_func){
  mcnemar_results <- tibble(reader_id = c(1:7), p_value = 0)
  for (i in 1:7) {
    mcnemar_results$p_value[i] <- test_func(df_processed, i)
  }
  mcnemar_results[is.na(mcnemar_results)] <- 1
  return(mcnemar_results)
}
mcnemar.AUC <- function(df_processed, i) {
  temp <- df_processed %>% 
    mutate(AIDED=1*(AIDED=="TP") + 1*(AIDED=="TN"),
           UNAIDED=1*(UNAIDED=="TP") + 1*(UNAIDED=="TN"))
  temp$AIDED <- as.factor(temp$AIDED)
  temp$UNAIDED <- as.factor(temp$UNAIDED)
  tb <- with(temp %>% filter(reader_id==i),
             table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}
mcnemar.CDR <- function(df_processed, i) {
  temp <- df_processed %>% 
    mutate(AIDED=1*(AIDED=="TP"),
           UNAIDED=1*(UNAIDED=="TP"))
  temp$AIDED <- as.factor(temp$AIDED)
  temp$UNAIDED <- as.factor(temp$UNAIDED)
  tb <- with(temp %>% filter(reader_id==i, is_cancer == 1),
             table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}
mcnemar.Specificity <- function(df_processed, i) {
  temp <- df_processed %>% 
    mutate(AIDED=1*(AIDED=="TN"),
           UNAIDED=1*(UNAIDED=="TN"))
  temp$AIDED <- as.factor(temp$AIDED)
  temp$UNAIDED <- as.factor(temp$UNAIDED)
  tb <- with(temp %>% filter(reader_id==i, is_cancer == 0),
             table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}
mcnemar.RecallRate <- function(df_processed, i) {
  temp <- df_processed %>% 
    mutate(AIDED = 1*(AIDED=="TP") + 1*(AIDED=="FP"),
           UNAIDED = 1*(UNAIDED=="TP") + 1*(UNAIDED=="FP"))
  temp$AIDED <- as.factor(temp$AIDED)
  temp$UNAIDED <- as.factor(temp$UNAIDED)
  tb <- with(temp %>% filter(reader_id==i),
             table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}
mcnemar.AUC.d <- function(df_processed, i) {
  temp <- df_processed %>% 
    mutate(AIDED=1*(AIDED=="TP") + 1*(AIDED=="TN"),
           UNAIDED=1*(UNAIDED=="TP") + 1*(UNAIDED=="TN"))
  temp$AIDED <- as.factor(temp$AIDED)
  temp$UNAIDED <- as.factor(temp$UNAIDED)
  tb <- with(temp %>% filter(difficulty_level==i),
             table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}
mcnemar.CDR.d <- function(df_processed, i) {
  temp <- df_processed %>% 
    mutate(AIDED=1*(AIDED=="TP"),
           UNAIDED=1*(UNAIDED=="TP"))
  temp$AIDED <- as.factor(temp$AIDED)
  temp$UNAIDED <- as.factor(temp$UNAIDED)
  tb <- with(temp %>% filter(difficulty_level==i, is_cancer == 1),
             table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}
mcnemar.Specificity.d <- function(df_processed, i) {
  temp <- df_processed %>% 
    mutate(AIDED=1*(AIDED=="TN"),
           UNAIDED=1*(UNAIDED=="TN"))
  temp$AIDED <- as.factor(temp$AIDED)
  temp$UNAIDED <- as.factor(temp$UNAIDED)
  tb <- with(temp %>% filter(difficulty_level==i, is_cancer == 0),
             table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}
mcnemar.RecallRate.d <- function(df_processed, i) {
  temp <- df_processed %>% 
    mutate(AIDED = 1*(AIDED=="TP") + 1*(AIDED=="FP"),
           UNAIDED = 1*(UNAIDED=="TP") + 1*(UNAIDED=="FP"))
  temp$AIDED <- as.factor(temp$AIDED)
  temp$UNAIDED <- as.factor(temp$UNAIDED)
  tb <- with(temp %>% filter(difficulty_level==i),
             table(AIDED, UNAIDED))
  return(mcnemar.test(tb)[["p.value"]])
}
```


# AUC
```{r}
df_AUC <- df_processed %>%
  group_by(reader_id) %>%
  summarise(unaided_mean=0,
            unaided_low=0,
            unaided_high=0,
            aided_mean=0,
            aided_low=0,
            aided_high=0,
            p_value_1=0,
            McNemar_p_value=0,
            .groups = 'drop'
            )
AUC <- function(data, indices) {
  data <- data[indices,]
  TP <- nrow(data %>% filter(detection=="TP"))
  total <- nrow(data %>% filter(is_cancer==1))
  TPR <- TP/total
  TN <- nrow(data %>% filter(detection=="TN"))
  total <- nrow(data %>% filter(is_cancer==0))
  specificity <-  TN/total
  FPR <- 1 - specificity
  auc <- 0.5*FPR*TPR + specificity*TPR
  return(auc)
}

for (i in 1:7){
  reader_aided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, AIDED) %>% 
    rename(detection=AIDED)
  reader_unaided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, UNAIDED) %>% 
    rename(detection=UNAIDED)
  results_unaided <- boot(data=reader_unaided, statistic=AUC, R=n)
  results_aided <- boot(data=reader_aided, statistic=AUC, R=n)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_AUC[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_AUC[i,"aided_mean"] <- ci_aided[["t0"]]
  df_AUC[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_AUC[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_AUC[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_AUC[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_AUC[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
}
```
## Statistical Tests
### Bootstrapping and Tests for Each Reader

```{r}
df_processed_AUC <- df_processed %>% 
  mutate(AIDED=1*(AIDED=="TP") + 1*(AIDED=="TN"),
         UNAIDED=1*(UNAIDED=="TP") + 1*(UNAIDED=="TN"))
for (i in 1:7){
  tb <- with(df_processed_AUC %>% filter(reader_id==i), table(AIDED, UNAIDED))
  df_AUC$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
```

```{r,results='asis'}
kable(df_AUC, caption = "bootstrapping results and p values of all tests for each reader's AUC")
```
We can see that **reader 1**, **reader 4**, **reader 5**, **reader 7** have significant different p values in McNemar's test. And **reader 4**, **reader 5**, **reader 7** are also significant in Hoanh's test. Let's see the two-way table for these readers for unaided and aided.

#### Reader 1

```{r}
with(df_processed_AUC %>% filter(reader_id==1), table(AIDED, UNAIDED))
```

We can see that aiding has a significant positive effect on **reader 1**'s AUC.

#### Reader 4

```{r}
with(df_processed_AUC %>% filter(reader_id==4), table(AIDED, UNAIDED))
```

We can see that aiding has a significant positive effect on **reader 4**'s AUC.

#### Reader 5

```{r}
with(df_processed_AUC %>% filter(reader_id==5), table(AIDED, UNAIDED))
```

We can see that aiding has a significant positive effect on **reader 5**'s AUC.

#### Reader 7

```{r}
with(df_processed_AUC %>% filter(reader_id==7), table(AIDED, UNAIDED))
```

We can see that aiding has a significant positive effect on **reader 7**'s AUC.

### Paired t test (one tailed) and McNemar's test for the whole group

```{r}
t_test_AUC <- t.test(df_AUC$unaided_mean, 
                  df_AUC$aided_mean, 
                  paired = TRUE, 
                  alternative = "less")
t_test_AUC
```

The p value of t test is `r t_test_AUC$p.value`, which is smaller than the significance level $\alpha=0.05$. We can conclude that aiding has significant positive effect on AUC of the whole reader group from paired t test. 

```{r}
tb <- with(df_processed_AUC, table(AIDED, UNAIDED))
mcnemar_test_AUC <- mcnemar.test(tb)
mcnemar_test_AUC
```

The p value of McNemar's test is `r mcnemar_test_AUC$p.value`, which is smaller than the significance level $\alpha=0.05$. We can conclude that aiding has significant positive effect on AUC of the whole reader group from paired t test. 

\newpage

# CDR
```{r}
df_CDR <- df_processed %>%
  group_by(reader_id) %>%
  summarise(unaided_mean=0,
            unaided_low=0,
            unaided_high=0,
            aided_mean=0,
            aided_low=0,
            aided_high=0,
            p_value_1=0,
            McNemar_p_value=0,
            .groups = 'drop'
            )
CDR <- function(data, indices) {
  data <- data[indices,]
  TP=nrow(data %>% filter(detection=="TP"))
  total=nrow(data %>% filter(is_cancer==1))
  return(TP/total)
}

for (i in 1:7){
  reader_aided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, AIDED) %>% 
    rename(detection=AIDED)
  reader_unaided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, UNAIDED) %>% 
    rename(detection=UNAIDED)
  results_unaided <- boot(data=reader_unaided, statistic=CDR, R=n)
  results_aided <- boot(data=reader_aided, statistic=CDR, R=n)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_CDR[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_CDR[i,"aided_mean"] <- ci_aided[["t0"]]
  df_CDR[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_CDR[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_CDR[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_CDR[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_CDR[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
}
```
## Statistical Tests
### Bootstrapping and Tests for Each Reader

```{r}
df_processed_CDR <- df_processed %>% 
  mutate(AIDED=1*(AIDED=="TP"),
         UNAIDED=1*(UNAIDED=="TP"))
for (i in 1:7){
  tb <- with(df_processed_CDR %>% filter(reader_id==i,is_cancer==1), table(AIDED, UNAIDED))
  df_CDR$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
```

```{r,results='asis'}
kable(df_CDR, caption = "bootstrapping results and p values of all tests for each reader's CDR")
```

We can see that **reader 1**, **reader 4**, **reader 5**, **reader 7** have significant different p values in McNemar's test. And **reader 4**, **reader 5**, **reader 7** are also significant in Hoanh's test. Let's see the two-way table for these readers for unaided and aided.

#### Reader 1

```{r}
with(df_processed_CDR %>% filter(reader_id==1,is_cancer==1), table(AIDED, UNAIDED))
```

We can see that aiding has a significant positive effect on **reader 1**'s CDR.

#### Reader 4

```{r}
with(df_processed_CDR %>% filter(reader_id==4,is_cancer==1), table(AIDED, UNAIDED))
```

We can see that aiding has a significant positive effect on **reader 4**'s CDR.

#### Reader 5

```{r}
with(df_processed_CDR %>% filter(reader_id==5,is_cancer==1), table(AIDED, UNAIDED))
```

We can see that aiding has a significant positive effect on **reader 5**'s CDR.

#### Reader 7

```{r}
with(df_processed_CDR %>% filter(reader_id==7,is_cancer==1), table(AIDED, UNAIDED))
```

We can see that aiding has a significant positive effect on **reader 7**'s CDR.

### Paired t test (one tailed) and McNemar's test for the whole group

```{r}
t_test_CDR <- t.test(df_CDR$unaided_mean,
                     df_CDR$aided_mean,
                     paired = TRUE,
                     alternative = "less")
t_test_CDR
```

The p value is `r t_test_CDR$p.value`, which is smaller than the significance level $\alpha=0.05$. We can conclude that aiding has significant positive effect on CDR of the whole reader group from paired t test. 

```{r}
tb <- with(df_processed_CDR %>% filter(is_cancer==1), table(AIDED, UNAIDED))
mcnemar_test_CDR <- mcnemar.test(tb)
mcnemar_test_CDR
```

The p value of McNemar's test is `r mcnemar_test_CDR$p.value`, which is smaller than the significance level $\alpha=0.05$. We can conclude that aiding has significant positive effect on AUC of the whole reader group from McNemar's test. 


\newpage
# Specificity
```{r}
df_Specificity <- df_processed %>%
  group_by(reader_id) %>%
  summarise(unaided_mean=0,
            unaided_low=0,
            unaided_high=0,
            aided_mean=0,
            aided_low=0,
            aided_high=0,
            p_value_1=0,
            McNemar_p_value=0,
            .groups = 'drop'
            )
Specificity <- function(data, indices) {
  data <- data[indices,]
  TN=nrow(data %>% filter(detection=="TN"))
  total=nrow(data %>% filter(is_cancer==0))
  return(TN/total)
}

for (i in 1:7){
  reader_aided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, AIDED) %>% 
    rename(detection=AIDED)
  reader_unaided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(is_cancer, UNAIDED) %>% 
    rename(detection=UNAIDED)
  results_unaided <- boot(data=reader_unaided, statistic=Specificity, R=n)
  results_aided <- boot(data=reader_aided, statistic=Specificity, R=n)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_Specificity[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_Specificity[i,"aided_mean"] <- ci_aided[["t0"]]
  df_Specificity[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_Specificity[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_Specificity[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_Specificity[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_Specificity[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
}
```
## Statistical Tests
### Bootstrapping and Tests for Each Reader

```{r}
df_processed_Specificity <- df_processed %>% 
  mutate(AIDED=1*(AIDED=="TN"),
         UNAIDED=1*(UNAIDED=="TN"))
df_processed_Specificity$AIDED <- as.factor(df_processed_Specificity$AIDED)
df_processed_Specificity$UNAIDED <- as.factor(df_processed_Specificity$UNAIDED)
for (i in 1:7){
  tb <- with(df_processed_Specificity %>% filter(reader_id==i,is_cancer==0), table(AIDED, UNAIDED))
  df_Specificity$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
df_Specificity[is.na(df_Specificity)] <- 1
```

```{r,results='asis'}
kable(df_Specificity, caption = "bootstrapping results and p values of all tests for each reader's Specificity")
```

We can see that no reader has a significant p value from either tests.

### Paired t test (one tailed) and McNemar's test for the whole group

```{r}
t_test_Specificity <- t.test(df_Specificity$unaided_mean,
                          df_Specificity$aided_mean,
                          paired = TRUE,
                          alternative = "less")
t_test_Specificity
```

The p value is `r t_test_Specificity$p.value`, which is larger than the significance level $\alpha=0.05$. We can conclude that aiding doesn't have significant positive effect on reader's Specificity from paired t test. 

```{r}
tb <- with(df_processed_Specificity %>% filter(is_cancer==0), table(AIDED, UNAIDED))
mcnemar_test_Specificity <- mcnemar.test(tb)
mcnemar_test_Specificity
```

The p value of McNemar's test is `r mcnemar_test_Specificity$p.value`, which is larger than the significance level $\alpha=0.05$. We can conclude that aiding doesn't have significant positive effect on reader's Specificity from McNemar's test. 


\newpage
# Recall Rate
```{r}
df_RecallRate <- df_processed %>%
  group_by(reader_id) %>%
  summarise(unaided_mean=0,
            unaided_low=0,
            unaided_high=0,
            aided_mean=0,
            aided_low=0,
            aided_high=0,
            p_value_1=0,
            McNemar_p_value=0,
            .groups = 'drop'
            )
RecallRate <- function(data, indices) {
  data <- data[indices,]
  RC=nrow(data %>% filter(detection=="TP" | detection=="FP"))
  total=nrow(data)
  return(RC/total)
}

for (i in 1:7){
  reader_aided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(AIDED) %>% 
    rename(detection=AIDED)
  reader_unaided <- 
    df_processed %>% 
    filter(reader_id==i) %>% 
    select(UNAIDED) %>% 
    rename(detection=UNAIDED)
  results_unaided <- boot(data=reader_unaided, statistic=RecallRate, R=n)
  results_aided <- boot(data=reader_aided, statistic=RecallRate, R=n)
  ci_unaided <- boot.ci(results_unaided, type="bca")
  ci_aided <- boot.ci(results_aided, type="bca")
  df_RecallRate[i,"unaided_mean"] <- ci_unaided[["t0"]]
  df_RecallRate[i,"aided_mean"] <- ci_aided[["t0"]]
  df_RecallRate[i,"unaided_low"] <- ci_unaided[["bca"]][4]
  df_RecallRate[i,"aided_low"] <- ci_aided[["bca"]][4]
  df_RecallRate[i,"unaided_high"] <- ci_unaided[["bca"]][5]
  df_RecallRate[i,"aided_high"] <- ci_aided[["bca"]][5]
  df_RecallRate[i,"p_value_1"] <- p_diff_1(results_unaided, results_aided)
}
```
## Statistical Tests
### Bootstrapping and Tests for Each Reader

```{r}
df_processed_RecallRate <-
  df_processed %>%
  mutate(AIDED = 1*(AIDED=="TP") + 1*(AIDED=="FP"),
         UNAIDED = 1*(UNAIDED=="TP") + 1*(UNAIDED=="FP"))
for (i in 1:7){
  tb <- with(df_processed_RecallRate %>% filter(reader_id==i), table(AIDED, UNAIDED))
  df_RecallRate$McNemar_p_value[i] <- mcnemar.test(tb)[["p.value"]]
}
```

```{r,results='asis'}
kable(df_RecallRate, caption = "bootstrapping results and p values of all tests for each reader's Recall Rate")
```

We can see that **reader 4**, **reader 5**, **reader 7** have significant different p values from both tests. Let's see the two-way table for these readers for unaided and aided.

#### Reader 4

```{r}
with(df_processed_RecallRate %>% filter(reader_id==4), table(AIDED, UNAIDED))
```

We can see that adding will make **reader 1** recall more.

#### Reader 5

```{r}
with(df_processed_RecallRate %>% filter(reader_id==5), table(AIDED, UNAIDED))
```

We can see that adding will make **reader 5** recall more.

#### Reader 7

```{r}
with(df_processed_RecallRate %>% filter(reader_id==7), table(AIDED, UNAIDED))
```

We can see that adding will make **reader 7** recall more.

### Paired t test (one tailed) and McNemar's test for the whole group

```{r}
t_test_RecallRate <- t.test(df_RecallRate$unaided_mean,
                         df_RecallRate$aided_mean,
                         paired = TRUE,
                         alternative = "less")
t_test_RecallRate
```

The p value of paired t test is `r t_test_RecallRate$p.value`, which is smaller than the significance level $\alpha=0.05$. We can conclude that aiding has significant positive effect on recall rate of the whole reader group from paired t test. 

```{r}
tb <- with(df_processed_RecallRate %>% filter(reader_id==i), table(AIDED, UNAIDED))
mcnemar_test_RecallRate <- mcnemar.test(tb)
mcnemar_test_RecallRate
```

The p value of McNemar's test is `r mcnemar_test_RecallRate$p.value`, which is smaller than the significance level $\alpha=0.05$. We can conclude that aiding has significant positive effect on recall rate of the whole reader group from McNemar's test. 

\newpage

<!-- ```{r} -->
<!-- write_csv(bind_cols(df_AUC[,c(1,8,9)], -->
<!--                     df_AUC$aided_mean-df_AUC$unaided_mean, -->
<!--                     df_CDR[,8:9],  -->
<!--                     df_CDR$aided_mean-df_CDR$unaided_mean, -->
<!--                     df_Specificity[,8:9],  -->
<!--                     df_Specificity$aided_mean-df_Specificity$unaided_mean, -->
<!--                     df_RecallRate[,8:9], -->
<!--                     df_RecallRate$aided_mean-df_RecallRate$unaided_mean, -->
<!--                     ),  -->
<!--           "data/Early_Detection_p_value.csv") -->
<!-- Early_Detection_t_test <- data.frame("reader_id"="whole group") -->
<!-- Early_Detection_t_test <- as.data.frame(cbind(Early_Detection_t_test,  -->
<!--                                               t_test_AUC$p.value,  -->
<!--                                               mcnemar_test_AUC$p.value, -->
<!--                                               t_test_CDR$p.value,  -->
<!--                                               mcnemar_test_CDR$p.value, -->
<!--                                               t_test_Specificity$p.value,  -->
<!--                                               mcnemar_test_Specificity$p.value, -->
<!--                                               t_test_RecallRate$p.value, -->
<!--                                               mcnemar_test_RecallRate$p.value -->
<!--                                               ) -->
<!--                                         ) -->
<!-- write_csv(Early_Detection_t_test, "data/Early_Detection_tests.csv") -->
<!-- ``` -->










