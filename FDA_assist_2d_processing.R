library(plyr)
library(tidyverse)
library(boot)
library(knitr)
library(kableExtra)
set.seed(123)
library(readxl)
# redo_df <- read_excel("data/200712_Reader_Results_20210728_1.xlsx")
# 
# redo_cases <- unique(redo_df %>% 
#                        select(MRN, `text-input-reader-id`, `text-input-image-type`, `select-input-initial-case-level-bi-rads`, `select-input-case-birads`, `select-input-case-complexity`) %>% 
#                        rename(reader = `text-input-reader-id`,
#                               type = `text-input-image-type`,
#                               x = `select-input-initial-case-level-bi-rads`,
#                               BIRADS = `select-input-case-birads`,
#                               complexity = `select-input-case-complexity`))
# 
# nrow(redo_cases)
# 
# error_list <- read_csv("data/Reader_errors_with_missing_cases.csv") %>% 
#   rename(reader = "Reader ID",
#          type = "Read Type")
# missing <- anti_join(error_list, redo_cases, by=c("reader", "type", "MRN"))
# redo <- inner_join(error_list, redo_cases, by=c("reader", "type", "MRN"))
# 
# write_csv(missing, "data/missing_cases.csv")
# write_csv(redo, "data/match_cases.csv")

df <- read_csv("data/case_by_case_summary_12_03_2020.csv")
df_processed <- df %>%
  select(
    reader,
    `Cancer/Normal`,
    MRN,
    `BIRADS (AIDED)`,
    `BIRADS (UNAIDED)`,
    `has reader markings (AIDED)`,
    `has reader markings (UNAIDED)`,
    `reader markings in GT (AIDED)`,
    `reader markings in GT (UNAIDED)`
  ) %>%
  rename(
    reader_id = `reader`,
    is_cancer = `Cancer/Normal`,
    AIDED.BIRADS = `BIRADS (AIDED)`,
    UNAIDED.BIRADS = `BIRADS (UNAIDED)`,
    AIDED.MARKING = `has reader markings (AIDED)`,
    UNAIDED.MARKING = `has reader markings (UNAIDED)`,
    AIDED.MARKING.GT = `reader markings in GT (AIDED)`,
    UNAIDED.MARKING.GT = `reader markings in GT (UNAIDED)`,
  ) %>%
  # filter(!(MRN %in% error_list$MRN)) %>%
  mutate(is_cancer = case_when(is_cancer == "CANCER" ~ 1,
                               is_cancer == "NORMAL" ~ 0)) %>%
  mutate(AIDED.BIRADS = case_when(AIDED.BIRADS == '1' ~ 1,
                                  AIDED.BIRADS == '2' ~ 2,
                                  AIDED.BIRADS == '3' ~ 3,
                                  AIDED.BIRADS == '4a' ~ 4,
                                  AIDED.BIRADS == '4b' ~ 4,
                                  AIDED.BIRADS == '4c' ~ 4,
                                  AIDED.BIRADS == '5' ~ 5)) %>%
  mutate(UNAIDED.BIRADS = case_when(UNAIDED.BIRADS == '1' ~ 1,
                                    UNAIDED.BIRADS == '2' ~ 2,
                                    UNAIDED.BIRADS == '3' ~ 3,
                                    UNAIDED.BIRADS == '4a' ~ 4,
                                    UNAIDED.BIRADS == '4b' ~ 4,
                                    UNAIDED.BIRADS == '4c' ~ 4,
                                    UNAIDED.BIRADS == '5' ~ 5))
na.unaided <- c(1:nrow(df_processed))[is.na(df_processed$UNAIDED.BIRADS)]
na.aided <- c(1:nrow(df_processed))[is.na(df_processed$AIDED.BIRADS)]
df_processed[na.unaided,]$UNAIDED.MARKING <- NA
df_processed[na.unaided,]$UNAIDED.MARKING.GT <- NA
df_processed$UNAIDED.MARKING <- as.logical(df_processed$UNAIDED.MARKING)
df_processed$UNAIDED.MARKING.GT <- as.logical(df_processed$UNAIDED.MARKING.GT)
df_processed[na.unaided,]$UNAIDED.BIRADS <- 4
df_processed[na.unaided,]$UNAIDED.MARKING <- TRUE
df_processed[na.unaided,]$UNAIDED.MARKING.GT <- TRUE
df_processed[na.aided,]$AIDED.BIRADS <- 1
df_processed[na.aided,]$AIDED.MARKING <- FALSE
df_processed[na.aided,]$AIDED.MARKING.GT <- FALSE

df_processed <- df_processed %>%
  mutate(AIDED.recall4 = case_when(AIDED.BIRADS < 4 ~ 0,
                                   AIDED.BIRADS >= 4 ~ 1,)) %>%
  mutate(UNAIDED.recall4 = case_when(UNAIDED.BIRADS < 4 ~ 0,
                                     UNAIDED.BIRADS >= 4 ~ 1,)) %>%
  mutate(AIDED.recall3 = case_when(AIDED.BIRADS < 3 ~ 0,
                                   AIDED.BIRADS >= 3 ~ 1,)) %>%
  mutate(UNAIDED.recall3 = case_when(UNAIDED.BIRADS < 3 ~ 0,
                                     UNAIDED.BIRADS >= 3 ~ 1,))

df_processed <- df_processed %>%
  mutate(UNAIDED.BIRADS.L = case_when(
    is_cancer==0~ UNAIDED.BIRADS,
    is_cancer==1 & UNAIDED.MARKING == FALSE ~ UNAIDED.BIRADS,
    is_cancer==1 & UNAIDED.MARKING == TRUE & UNAIDED.MARKING.GT == TRUE ~ UNAIDED.BIRADS,
    is_cancer==1 & UNAIDED.MARKING == TRUE & UNAIDED.MARKING.GT == FALSE ~ 1)) %>%
  mutate(AIDED.BIRADS.L = case_when(
    is_cancer==0~ AIDED.BIRADS,
    is_cancer==1 & AIDED.MARKING == FALSE ~ AIDED.BIRADS,
    is_cancer==1 & AIDED.MARKING == TRUE & AIDED.MARKING.GT == TRUE ~ AIDED.BIRADS,
    is_cancer==1 & AIDED.MARKING == TRUE & AIDED.MARKING.GT == FALSE ~ 1))

df_processed <- df_processed %>%
  mutate(AIDED.recall4.L = case_when(AIDED.BIRADS.L < 4 ~ 0,
                                   AIDED.BIRADS.L >= 4 ~ 1,)) %>%
  mutate(UNAIDED.recall4.L = case_when(UNAIDED.BIRADS.L < 4 ~ 0,
                                     UNAIDED.BIRADS.L >= 4 ~ 1,)) %>%
  mutate(AIDED.recall3.L = case_when(AIDED.BIRADS.L < 3 ~ 0,
                                   AIDED.BIRADS.L >= 3 ~ 1,)) %>%
  mutate(UNAIDED.recall3.L = case_when(UNAIDED.BIRADS.L < 3 ~ 0,
                                     UNAIDED.BIRADS.L >= 3 ~ 1,))

reader_score <- df_processed %>% select(MRN, reader_id, is_cancer, UNAIDED.recall3, AIDED.recall3)
names(reader_score)[4] <- "unaided"
names(reader_score)[5] <- "aided"
reader_score <- gather(reader_score, "modality", "rating", 4:5)
names(reader_score)[1] <- "case_id"
names(reader_score)[3] <- "GT"
write_csv(reader_score, "cmAssist_2d_FDA_recall3.csv")

reader_score <- df_processed %>% select(MRN, reader_id, is_cancer, UNAIDED.recall4, AIDED.recall4)
names(reader_score)[4] <- "unaided"
names(reader_score)[5] <- "aided"
reader_score <- gather(reader_score, "modality", "rating", 4:5)
names(reader_score)[1] <- "case_id"
names(reader_score)[3] <- "GT"
write_csv(reader_score, "cmAssist_2d_FDA_recall4.csv")

reader_score <- df_processed %>% select(MRN, reader_id, is_cancer, UNAIDED.recall3.L, AIDED.recall3.L)
names(reader_score)[4] <- "unaided"
names(reader_score)[5] <- "aided"
reader_score <- gather(reader_score, "modality", "rating", 4:5)
names(reader_score)[1] <- "case_id"
names(reader_score)[3] <- "GT"
write_csv(reader_score, "cmAssist_2d_FDA_recall3_L.csv")

reader_score <- df_processed %>% select(MRN, reader_id, is_cancer, UNAIDED.recall4.L, AIDED.recall4.L)
names(reader_score)[4] <- "unaided"
names(reader_score)[5] <- "aided"
reader_score <- gather(reader_score, "modality", "rating", 4:5)
names(reader_score)[1] <- "case_id"
names(reader_score)[3] <- "GT"
write_csv(reader_score, "cmAssist_2d_FDA_recall4_L.csv")
